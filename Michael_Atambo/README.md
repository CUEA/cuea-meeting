#Basic Yambo Tutoroal

## Neccessary files
0. The `training.pdf` file in this directory
1. Hands on files:  http://www.yambo-code.org/educational/tutorials/files/hBN.tar.gz
2. Source Code:  https://github.com/yambo-code/yambo/archive/4.5.0.tar.gz  

## Procedure
1. Install yambo as directed in  `training.pdf`
2. Ensure you have expresso installed. 
3. Untar the downloaded tutorial file: `tar -xvf hBN.tar.gz`
3. Perform an `SCF` anf `NSCF` calculation using QE in the  PWSCF directory of the  untarred file
4. Peform a conversion from QE to YAMBO databases using `p2y`, which is provided by yambo.
5. Perform an initialization (as directed in `training.pdf`)
6. Follow the instructions to do a 1-shot GW calculation
7. Follow the instructions to perform an RPA optics calculation.
8. Follow the instructions to perform a GW-BSE optics calculation.

## Quantum Mobile Virtual Machine. 
* A virtual Machine is available with all the software preconfigured.
* It can be downloaded from http://bit.ly/2Ej5bWm (3.5 G)
* the username is `max` and the password is `moritz`
* Later versions can be found at: https://github.com/marvel-nccr/quantum-mobile/releases
* you will need to ensure virtualbox is installed in your machine (www.virtualbox.org)
