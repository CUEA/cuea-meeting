#
SIESTA_ARCH=gfortran-mpi-example
WITH_MPI=1
WITH_NETCDF=1
bundle_root=/mnt/lustre/users/kwamalwakhaemba/siesta-bundle/build_dir/install
NETCDF_ROOT=$(bundle_root)
PSML_ROOT=$(bundle_root)
XMLF90_ROOT=$(bundle_root)
GRIDXC_ROOT=$(bundle_root)
LIBXC_ROOT=$(bundle_root)
SCALAPACK_LIBS=/apps/scalapack/gcc/current/lib64/libscalapack.a /apps/blacs/openmpi/gcc/current/lib64/libblacs.a /apps/blacs/openmpi/gcc/current/lib64/libblacsCinit.a /apps/blacs/openmpi/gcc/current/lib64/libblacsF77init.a
COMP_LIBS = libsiestaLAPACK.a libsiestaBLAS.a  # Generic built-in
FC_PARALLEL=mpif90
FC_SERIAL=gfortran
FC_ASIS=$(FC)
FFLAGS= -O2 -fbacktrace #-fimplicit-none
FFLAGS_CHECKS= -O0 -g -fcheck=all -Warray-temporaries
FFLAGS_DEBUG= -g -O0
RANLIB=echo
ifdef WITH_NETCDF
 ifndef NETCDF_ROOT
   $(error you need to define NETCDF_ROOT in your arch.make)
 endif
 NETCDF_INCFLAGS=-I$(NETCDF_ROOT)/include
 NETCDF_LIBS= -L$(NETCDF_ROOT)/lib -lnetcdff -lnetcdf
 FPPFLAGS_CDF= -DCDF
 LIBS +=$(NETCDF_LIBS)
endif
ifdef WITH_MPI
 FC=$(FC_PARALLEL)
 MPI_INTERFACE=libmpi_f90.a
 MPI_INCLUDE=.      # Note . for no-op
 FPPFLAGS_MPI=-DMPI -DMPI_TIMING
 LIBS +=$(SCALAPACK_LIBS)
else
 FC=$(FC_SERIAL)
endif

LIBS += $(LAPACK_LIBS) $(COMP_LIBS)

SYS=nag
FPPFLAGS= $(FPPFLAGS_CDF) $(FPPFLAGS_MPI)  -DF2003 
#
#---------------------------------------------
include $(XMLF90_ROOT)/share/org.siesta-project/xmlf90.mk
include $(PSML_ROOT)/share/org.siesta-project/psml.mk
include $(GRIDXC_ROOT)/share/org.siesta-project/gridxc.mk
#---------------------------------------------
#
.F.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS)  $(FPPFLAGS) $<
.f.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS)   $<
.F90.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS)  $(FPPFLAGS) $<
.f90.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS)   $<
#
