\contentsline {section}{Contributors to \siesta }{2}{section*.1}
\contentsline {section}{\numberline {1}INTRODUCTION}{8}{section.1}
\contentsline {section}{\numberline {2}COMPILATION}{10}{section.2}
\contentsline {subsection}{\numberline {2.1}The build directory}{10}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Multiple-target compilation}{11}{subsubsection.2.1.1}
\contentsline {subsection}{\numberline {2.2}The arch.make file}{11}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Parallel}{12}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}MPI}{12}{subsubsection.2.3.1}
\contentsline {subsubsection}{\numberline {2.3.2}OpenMP}{12}{subsubsection.2.3.2}
\contentsline {subsection}{\numberline {2.4}Library dependencies}{13}{subsection.2.4}
\contentsline {section}{\numberline {3}EXECUTION OF THE PROGRAM}{17}{section.3}
\contentsline {subsection}{\numberline {3.1}Specific execution options}{19}{subsection.3.1}
\contentsline {section}{\numberline {4}THE FLEXIBLE DATA FORMAT (FDF)}{20}{section.4}
\contentsline {section}{\numberline {5}PROGRAM OUTPUT}{21}{section.5}
\contentsline {subsection}{\numberline {5.1}Standard output}{21}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Output to dedicated files}{22}{subsection.5.2}
\contentsline {section}{\numberline {6}DETAILED DESCRIPTION OF PROGRAM OPTIONS}{22}{section.6}
\contentsline {subsection}{\numberline {6.1}General system descriptors}{22}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Pseudopotentials}{24}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Basis set and KB projectors}{24}{subsection.6.3}
\contentsline {subsubsection}{\numberline {6.3.1}Overview of atomic-orbital bases implemented in \siesta }{24}{subsubsection.6.3.1}
\contentsline {subsubsection}{\numberline {6.3.2}Type of basis sets}{28}{subsubsection.6.3.2}
\contentsline {subsubsection}{\numberline {6.3.3}Size of the basis set}{29}{subsubsection.6.3.3}
\contentsline {subsubsection}{\numberline {6.3.4}Range of the orbitals}{29}{subsubsection.6.3.4}
\contentsline {subsubsection}{\numberline {6.3.5}Generation of multiple-zeta orbitals}{30}{subsubsection.6.3.5}
\contentsline {subsubsection}{\numberline {6.3.6}Soft-confinement options}{31}{subsubsection.6.3.6}
\contentsline {subsubsection}{\numberline {6.3.7}Kleinman-Bylander projectors}{31}{subsubsection.6.3.7}
\contentsline {subsubsection}{\numberline {6.3.8}The PAO.Basis block}{33}{subsubsection.6.3.8}
\contentsline {subsubsection}{\numberline {6.3.9}Filtering}{36}{subsubsection.6.3.9}
\contentsline {subsubsection}{\numberline {6.3.10}Saving and reading basis-set information}{36}{subsubsection.6.3.10}
\contentsline {subsubsection}{\numberline {6.3.11}Tools to inspect the orbitals and KB projectors}{37}{subsubsection.6.3.11}
\contentsline {subsubsection}{\numberline {6.3.12}Basis optimization}{37}{subsubsection.6.3.12}
\contentsline {subsubsection}{\numberline {6.3.13}Low-level options regarding the radial grid}{37}{subsubsection.6.3.13}
\contentsline {subsection}{\numberline {6.4}Structural information}{38}{subsection.6.4}
\contentsline {subsubsection}{\numberline {6.4.1}Traditional structure input in the fdf file}{39}{subsubsection.6.4.1}
\contentsline {subsubsection}{\numberline {6.4.2}Z-matrix format and constraints}{41}{subsubsection.6.4.2}
\contentsline {subsubsection}{\numberline {6.4.3}Output of structural information}{44}{subsubsection.6.4.3}
\contentsline {subsubsection}{\numberline {6.4.4}Input of structural information from external files}{45}{subsubsection.6.4.4}
\contentsline {subsubsection}{\numberline {6.4.5}Input from a FIFO file}{46}{subsubsection.6.4.5}
\contentsline {subsubsection}{\numberline {6.4.6}Precedence issues in structural input}{46}{subsubsection.6.4.6}
\contentsline {subsubsection}{\numberline {6.4.7}Interatomic distances}{46}{subsubsection.6.4.7}
\contentsline {subsection}{\numberline {6.5}$k$-point sampling}{47}{subsection.6.5}
\contentsline {subsubsection}{\numberline {6.5.1}Output of k-point information}{48}{subsubsection.6.5.1}
\contentsline {subsection}{\numberline {6.6}Exchange-correlation functionals}{49}{subsection.6.6}
\contentsline {subsection}{\numberline {6.7}Spin polarization}{50}{subsection.6.7}
\contentsline {subsection}{\numberline {6.8}Spin-Orbit coupling}{51}{subsection.6.8}
\contentsline {subsection}{\numberline {6.9}The self-consistent-field loop}{53}{subsection.6.9}
\contentsline {subsubsection}{\numberline {6.9.1}Harris functional}{54}{subsubsection.6.9.1}
\contentsline {subsubsection}{\numberline {6.9.2}Mixing options}{55}{subsubsection.6.9.2}
\contentsline {subsubsection}{\numberline {6.9.3}Mixing of the Charge Density}{61}{subsubsection.6.9.3}
\contentsline {subsubsection}{\numberline {6.9.4}Initialization of the density-matrix}{63}{subsubsection.6.9.4}
\contentsline {subsubsection}{\numberline {6.9.5}Initialization of the SCF cycle with charge densities}{66}{subsubsection.6.9.5}
\contentsline {subsubsection}{\numberline {6.9.6}Output of density matrix and Hamiltonian}{66}{subsubsection.6.9.6}
\contentsline {subsubsection}{\numberline {6.9.7}Convergence criteria}{68}{subsubsection.6.9.7}
\contentsline {subsection}{\numberline {6.10}The real-space grid and the eggbox-effect}{70}{subsection.6.10}
\contentsline {subsection}{\numberline {6.11}Matrix elements of the Hamiltonian and overlap}{73}{subsection.6.11}
\contentsline {subsubsection}{\numberline {6.11.1}The auxiliary supercell}{74}{subsubsection.6.11.1}
\contentsline {subsection}{\numberline {6.12}Calculation of the electronic structure}{74}{subsection.6.12}
\contentsline {subsubsection}{\numberline {6.12.1}Diagonalization options}{75}{subsubsection.6.12.1}
\contentsline {paragraph}{Deprecated diagonalization options}{78}{section*.3}
\contentsline {subsubsection}{\numberline {6.12.2}Output of eigenvalues and wavefunctions}{79}{subsubsection.6.12.2}
\contentsline {subsubsection}{\numberline {6.12.3}Occupation of electronic states and Fermi level}{79}{subsubsection.6.12.3}
\contentsline {subsubsection}{\numberline {6.12.4}Orbital minimization method (OMM)}{80}{subsubsection.6.12.4}
\contentsline {subsubsection}{\numberline {6.12.5}Order(N) calculations}{82}{subsubsection.6.12.5}
\contentsline {paragraph}{Output of localized wavefunctions}{84}{section*.4}
\contentsline {subsection}{\numberline {6.13}The CheSS solver}{84}{subsection.6.13}
\contentsline {subsubsection}{\numberline {6.13.1}Input parameters}{84}{subsubsection.6.13.1}
\contentsline {subsection}{\numberline {6.14}The PEXSI solver}{85}{subsection.6.14}
\contentsline {subsubsection}{\numberline {6.14.1}Pole handling}{85}{subsubsection.6.14.1}
\contentsline {subsubsection}{\numberline {6.14.2}Parallel environment and control options}{86}{subsubsection.6.14.2}
\contentsline {subsubsection}{\numberline {6.14.3}Electron tolerance and the PEXSI solver}{87}{subsubsection.6.14.3}
\contentsline {subsubsection}{\numberline {6.14.4}Inertia-counting}{88}{subsubsection.6.14.4}
\contentsline {subsubsection}{\numberline {6.14.5}Re-use of $\mu $ information accross iterations}{89}{subsubsection.6.14.5}
\contentsline {subsubsection}{\numberline {6.14.6}Calculation of the density of states by inertia-counting}{90}{subsubsection.6.14.6}
\contentsline {subsubsection}{\numberline {6.14.7}Calculation of the LDOS by selected-inversion}{91}{subsubsection.6.14.7}
\contentsline {subsection}{\numberline {6.15}Band-structure analysis}{91}{subsection.6.15}
\contentsline {subsubsection}{\numberline {6.15.1}Format of the .bands file}{92}{subsubsection.6.15.1}
\contentsline {subsubsection}{\numberline {6.15.2}Output of wavefunctions associated to bands}{93}{subsubsection.6.15.2}
\contentsline {subsection}{\numberline {6.16}Output of selected wavefunctions}{93}{subsection.6.16}
\contentsline {subsection}{\numberline {6.17}Density of states}{94}{subsection.6.17}
\contentsline {subsubsection}{\numberline {6.17.1}Total density of states}{94}{subsubsection.6.17.1}
\contentsline {subsubsection}{\numberline {6.17.2}Partial (projected) density of states}{95}{subsubsection.6.17.2}
\contentsline {subsubsection}{\numberline {6.17.3}Local density of states}{96}{subsubsection.6.17.3}
\contentsline {subsection}{\numberline {6.18}Options for chemical analysis}{97}{subsection.6.18}
\contentsline {subsubsection}{\numberline {6.18.1}Mulliken charges and overlap populations}{97}{subsubsection.6.18.1}
\contentsline {subsubsection}{\numberline {6.18.2}Voronoi and Hirshfeld atomic population analysis}{97}{subsubsection.6.18.2}
\contentsline {subsubsection}{\numberline {6.18.3}Crystal-Orbital overlap and hamilton populations (COOP/COHP)}{98}{subsubsection.6.18.3}
\contentsline {subsection}{\numberline {6.19}Optical properties}{99}{subsection.6.19}
\contentsline {subsection}{\numberline {6.20}Macroscopic polarization}{100}{subsection.6.20}
\contentsline {subsection}{\numberline {6.21}Maximally Localized Wannier Functions}{102}{subsection.6.21}
\contentsline {subsubsection}{\numberline {6.21.1}\program {wannier90} as a postprocessing tool}{102}{subsubsection.6.21.1}
\contentsline {subsubsection}{\numberline {6.21.2}\program {wannier90} called as a library within \program {siesta}}{104}{subsubsection.6.21.2}
\contentsline {subsection}{\numberline {6.22}Systems with net charge or dipole, and electric fields}{108}{subsection.6.22}
\contentsline {subsection}{\numberline {6.23}Output of charge densities and potentials on the grid}{111}{subsection.6.23}
\contentsline {subsection}{\numberline {6.24}Auxiliary Force field}{113}{subsection.6.24}
\contentsline {subsection}{\numberline {6.25}Parallel options}{114}{subsection.6.25}
\contentsline {subsubsection}{\numberline {6.25.1}Parallel decompositions for O(N)}{114}{subsubsection.6.25.1}
\contentsline {subsection}{\numberline {6.26}Efficiency options}{115}{subsection.6.26}
\contentsline {subsection}{\numberline {6.27}Memory, CPU-time, and Wall time accounting options}{115}{subsection.6.27}
\contentsline {subsection}{\numberline {6.28}The catch-all option UseSaveData}{116}{subsection.6.28}
\contentsline {subsection}{\numberline {6.29}Output of information for Denchar}{117}{subsection.6.29}
\contentsline {subsection}{\numberline {6.30}NetCDF (CDF4) output file}{117}{subsection.6.30}
\contentsline {section}{\numberline {7}STRUCTURAL RELAXATION, PHONONS, AND MOLECULAR DYNAMICS}{118}{section.7}
\contentsline {subsection}{\numberline {7.1}Compatibility with pre-v4 versions}{119}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}Structural relaxation}{120}{subsection.7.2}
\contentsline {subsubsection}{\numberline {7.2.1}Conjugate-gradients optimization}{122}{subsubsection.7.2.1}
\contentsline {subsubsection}{\numberline {7.2.2}Broyden optimization}{122}{subsubsection.7.2.2}
\contentsline {subsubsection}{\numberline {7.2.3}FIRE relaxation}{122}{subsubsection.7.2.3}
\contentsline {subsection}{\numberline {7.3}Target stress options}{123}{subsection.7.3}
\contentsline {subsection}{\numberline {7.4}Molecular dynamics}{123}{subsection.7.4}
\contentsline {subsection}{\numberline {7.5}Output options for dynamics}{125}{subsection.7.5}
\contentsline {subsection}{\numberline {7.6}Restarting geometry optimizations and MD runs}{126}{subsection.7.6}
\contentsline {subsection}{\numberline {7.7}Use of general constraints}{126}{subsection.7.7}
\contentsline {subsection}{\numberline {7.8}Phonon calculations}{129}{subsection.7.8}
\contentsline {section}{\numberline {8}LDA+U}{130}{section.8}
\contentsline {section}{\numberline {9}RT-TDDFT}{132}{section.9}
\contentsline {subsection}{\numberline {9.1}Brief description}{132}{subsection.9.1}
\contentsline {subsection}{\numberline {9.2}Partial Occupations}{132}{subsection.9.2}
\contentsline {subsection}{\numberline {9.3}Input options for RT-TDDFT}{133}{subsection.9.3}
\contentsline {section}{\numberline {10}External control of \siesta }{134}{section.10}
\contentsline {subsection}{\numberline {10.1}Examples of Lua programs}{137}{subsection.10.1}
\contentsline {subsection}{\numberline {10.2}External MD/relaxation methods}{137}{subsection.10.2}
\contentsline {section}{\numberline {11}TRANSIESTA}{137}{section.11}
\contentsline {subsection}{\numberline {11.1}Source code structure}{137}{subsection.11.1}
\contentsline {subsection}{\numberline {11.2}Compilation}{137}{subsection.11.2}
\contentsline {subsection}{\numberline {11.3}Brief description}{138}{subsection.11.3}
\contentsline {subsection}{\numberline {11.4}Electrodes}{140}{subsection.11.4}
\contentsline {subsubsection}{\numberline {11.4.1}Matching coordinates}{140}{subsubsection.11.4.1}
\contentsline {paragraph}{Tile}{141}{section*.5}
\contentsline {paragraph}{Repeat}{141}{section*.6}
\contentsline {subsubsection}{\numberline {11.4.2}Principal layer interactions}{141}{subsubsection.11.4.2}
\contentsline {subsection}{\numberline {11.5}\tsiesta \ Options}{142}{subsection.11.5}
\contentsline {subsubsection}{\numberline {11.5.1}Quick and dirty}{142}{subsubsection.11.5.1}
\contentsline {subsubsection}{\numberline {11.5.2}General options}{143}{subsubsection.11.5.2}
\contentsline {subsection}{\numberline {11.6}$k$-point sampling}{148}{subsection.11.6}
\contentsline {subsubsection}{\numberline {11.6.1}Algorithm specific options}{148}{subsubsection.11.6.1}
\contentsline {subsubsection}{\numberline {11.6.2}Poisson solution for fixed boundary conditions}{150}{subsubsection.11.6.2}
\contentsline {subsubsection}{\numberline {11.6.3}Electrode description options}{151}{subsubsection.11.6.3}
\contentsline {subsubsection}{\numberline {11.6.4}Chemical potentials}{155}{subsubsection.11.6.4}
\contentsline {subsubsection}{\numberline {11.6.5}Complex contour integration options}{157}{subsubsection.11.6.5}
\contentsline {subsubsection}{\numberline {11.6.6}Bias contour integration options}{159}{subsubsection.11.6.6}
\contentsline {subsection}{\numberline {11.7}Output}{159}{subsection.11.7}
\contentsline {subsection}{\numberline {11.8}Utilities for analysis: \tbtrans }{160}{subsection.11.8}
\contentsline {section}{\numberline {12}ANALYSIS TOOLS}{160}{section.12}
\contentsline {section}{\numberline {13}SCRIPTING}{160}{section.13}
\contentsline {section}{\numberline {14}PROBLEM HANDLING}{161}{section.14}
\contentsline {subsection}{\numberline {14.1}Error and warning messages}{161}{subsection.14.1}
\contentsline {section}{\numberline {15}REPORTING BUGS}{161}{section.15}
\contentsline {section}{\numberline {16}ACKNOWLEDGMENTS}{161}{section.16}
\contentsline {section}{\numberline {17}APPENDIX: Physical unit names recognized by FDF}{163}{section.17}
\contentsline {section}{\numberline {18}APPENDIX: XML Output}{165}{section.18}
\contentsline {subsection}{\numberline {18.1}Controlling XML output}{165}{subsection.18.1}
\contentsline {subsection}{\numberline {18.2}Converting XML to XHTML}{165}{subsection.18.2}
\contentsline {section}{\numberline {19}APPENDIX: Selection of precision for storage}{166}{section.19}
\contentsline {section}{\numberline {20}APPENDIX: Data structures and reference counting}{167}{section.20}
\contentsline {section}{Bibliography}{168}{section.20}
