# 
# Copyright (C) 1996-2016	The SIESTA group
#  This file is distributed under the terms of the
#  GNU General Public License: see COPYING in the top directory
#  or http://www.gnu.org/copyleft/gpl.txt.
# See Docs/Contributors.txt for a list of contributors.
#
.SUFFIXES:
.SUFFIXES: .f .F .o .a .f90 .F90

SIESTA_ARCH=x86_64-unknown-linux-gnu--Gfortran

FPP=
FPP_OUTPUT= 
FC=gfortran
RANLIB=echo

SYS=nag

SP_KIND=4
DP_KIND=8
KINDS=$(SP_KIND) $(DP_KIND)

FFLAGS=-g -O2

FPPFLAGS=  -DFC_HAVE_FLUSH -DFC_HAVE_ABORT -DCDF -DGRID_DP -DPHI_GRID_SP -DHAVE_WANNIER90 -DHAVE_LIBINT
LDFLAGS=

ARFLAGS_EXTRA=

FCFLAGS_fixed_f=
FCFLAGS_free_f90=
FPPFLAGS_fixed_F=
FPPFLAGS_free_F90=

INCFLAGS=-I/home/james/programms/siesta-bundle/build_dir/install/include -I/usr/include -I. #-I/usr/lib/openmpi/include
BLAS_LIBS=/usr/lib/x86_64-linux-gnu/blas/libblas.a #-lblas
LAPACK_LIBS=/usr/lib/x86_64-linux-gnu/lapack/liblapack.a #-llapack

COMP_LIBS=


WANNIER90_ROOT = /home/james/programms/v3.0.0/wannier90-3.0.0
WANNIER90_INCFLAGS = -I$(WANNIER90_ROOT)/src/obj
WANNIER90_LIBS = -L$(WANNIER90_ROOT) -lwannier
DEFS_WANNIER90 = $(DEFS_PREFIX)-DHAVE_WANNIER90

NETCDF_INCFLAGS = -I/home/james/programms/siesta-bundle/build_dir/install/include
NETCDF_LIBS = -L/home/james/programms/siesta-bundle/build_dir/install/lib -lnetcdff -lnetcdf

LIBS=$(NETCDF_LIBS) $(WANNIER90_LIBS) -lpthread $(LAPACK_LIBS) $(BLAS_LIBS) 

#SIESTA needs an F90 interface to MPI
#This will give you SIESTA's own implementation
#If your compiler vendor offers an alternative, you may change
#to it here.
#MPI_INTERFACE=libmpi_f90.a
#MPI_INCLUDE=.

#Dependency rules are created by autoconf according to whether
#discrete preprocessing is necessary or not.
.F.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS) $(FPPFLAGS) $(FPPFLAGS_fixed_F)  $< 
.F90.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS) $(FPPFLAGS) $(FPPFLAGS_free_F90) $< 
.f.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS) $(FCFLAGS_fixed_f)  $<
.f90.o:
	$(FC) -c $(FFLAGS) $(INCFLAGS) $(FCFLAGS_free_f90)  $<


