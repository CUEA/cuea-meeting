*******************************************************************************
WARNING: BEFORE RUNNING A CALCULATION FOR PRODUCTION,
TEST THE PSEUDOPOTENTIAL AND BASIS SETS, AND PERFORM
THE CONVERGENCE TESTS (MESH CUTOFF, ETC.)

IN THE PRESENT EXAMPLES, AND IN ORDER TO SPEED THE CALCULATIONS,
WE PROVIDE SOME VALUES OF THESE PARAMETERS FOR YOU.

WE DO NOT WARRANTY THAT THE VALUES OF THESE PARAMETERS ARE CONVERGED.

THE RESULTS PRESENTED BELOW HAVE BEEN OBTAINED USING:
Version of Siesta: siesta-3.0-b (serial mode)
Compiler: g95 (version 0.92)
Compilation flags: FFLAGS= -O2 -Wall
                   FPPFLAGS= -DGRID_DP $(FPPFLAGS_CDF) $(FPPFLAGS_MPI)
Double precision for the grid variables enabled.
No linear algebra libraries used.
RESULTS MIGHT DIFFER SLIGHTLY DEPENDING ON THE PLATFORM, COMPILER,
AND COMPILATION FLAGS
*******************************************************************************

All the lines after the $ should be written by the user in the command line.


* Edit the input file, SrTiO3.fdf, and study the different variables.
Pay special attention to the new variables in this example, and
check their meaning in the User's Guide:
   BandLineScale
   %block BandLines

You can see the path chosen in k-space (%block BandLines) in this
example in the figure Brillouin_Zone.pdf in this folder.

* Select the lattice constant wich minimizes the total energy of the
system (in this case, you do not have to compute it; it is given to 
you and amounts to 3.874 Ang). Run the code setting the right lattice
constant in the input.
Once the input is ready, run a command like this:

$ siesta < SrTiO3.fdf > SrTiO3.out 

* Siesta writes the bands information in a file called SystemLabel.bands,
in this case SrTiO3.bands. Take a look at this file with a text editor:

  - The first line of the file is the energy of the Fermi level.
  - The numbers in the second line are minimum and maximum length of the path
    chosen in k-space.
  - The numbers in the third line are the minimum and maximum eigenvalue 
    energies of the bands diagram.
  - The numbers in the fourth line are the number of orbitals/bands 
    calculated, spin restricted (=1) or unrestricted (=2), and the
    total number of k-points where the bands have been calculated.
  - Then the bands are written for each k-point. Check that for each 
    k-point (first column) there are as many bands as the first number
    in the fourth line.
  - At the end of the file the high symmetry k-points are written.
    First the number of these points, and then in two columns, the 
    position in the path and their name.

* The file systemlabel.bands can be read using the file new.gnubands.f90 at
the folder Siesta-folder/Util/Bands, or the older version of the same 
Utility file (gnubands.f). 
We provide this last one within this example

To run it, simply compile it:

$ <your_Fortran_90_compiler> -o gnubands.x gnubands.f
$ gnubands.x < SrTiO3.bands > SrTiO3.bands.gnuplot.dat
$ gnuplot
$ gnuplot> plot "SrTiO3.bands.gnuplot.dat" using 1:2 with lines
$ gnuplot> set xrange [0.0:2.81]   # 2.81 is the position of last 
                                   # point in the path in k
$ gnuplot> set yrange [-70.0:0.0]  # this is large enough to include 
                                   # all the valence bands and the 
                                   # bottom of the conduction bands
$ gnuplot> replot

The result is a plot with the band diagram of SrTiO3, including those
bands coming from the Ti and Sr semicore states. 

To focus only on the top of the valence band, from gnuplot type
$ gnuplot> set yrange [-25.0:0.0]  # this is large enough to include 
                                   # O 2s and Ba 5p character bands
$ gnuplot> replot

* To produce a postscript file with one of the previous figures
First the number of these points, and then in two columns, the 

gnuplot> set terminal postscript color
gnuplot> set output "SrTiO3.bands.ps"
gnuplot> replot
gnuplot> quit

* To generate a pdf file from the previous postscript

$ ps2pdf SrTiO3.bands.ps


* Then you can argue if this system is an insulator, a semiconductor 
 or a metal. 

 What's the value of the gap?
 Compare it with the experimental value, 3.2 eV. 
 Is it direct or indirect? 

* Why the deep bands do not have any dispersion in k-space?


